<?php

$url = "https://api.stackexchange.com/2.2/questions?";

// Validate Page and Rpp are used together
if( $_POST['page'] && $_POST['rpp'] ){

	$url .= "page=" . $_POST['page'];
	$url .= "&pagesize=" .$_POST['rpp'];

} elseif  ( $_POST['page'] || $_POST['rpp'] ){

	exit("Page and Rpp must be used together");

}

// Add sort parameter if used
if($_POST['sort']){
	$url .= "&sort=" . $_POST['sort'];
}

// Add score parameter and flag if used
$score_flag = true;
$score = 0;
if($_POST['score']){
	$url .= "&score=" . $_POST['score'];
	$score = $_POST['score'];
	$score_flag = false;
}

$url .= "&site=stackoverflow";

// echo $url;

// GET json file from StackExchange
$curl = curl_init();
curl_setopt($curl, CURLOPT_URL, $url);
curl_setopt($curl, CURLOPT_ENCODING, 'gzip');
curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);

$data = curl_exec($curl);

curl_close($curl);

// Convert it to an Array
$content = json_decode(html_entity_decode($data),true);

if($content){

echo "<h3> Search Results: </h3>";
echo '<div><ol type="1">';

	foreach ($content['items'] as $question) {

		// when $score_flag = true the routine will always be executed
		if ($score_flag || ($question['score'] > $score) ){

			echo "<li>";

				echo "<ul>Question title: ".$question['title'] . " ";
				echo '<a href="'.$question['link'].'"> Link </a>' . "</ul>";

				echo "<ul>Author: ".$question['owner']['display_name'] . " ";
				echo '<a href="'.$question['owner']['link'].'">Profile</a>' . "</ul>";

				echo "<ul>Question score: ".$question['score'] . "</ul></br>";

			echo "</li>";
		}
		
	}

echo "</ol></div>";

} else{
	echo "<h3>Nothing to display</h3>";
}

?>