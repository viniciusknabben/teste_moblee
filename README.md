# README #

Teste para vaga de estágio na empresa Moblee.

## Desafio: 

* Fazer uma aplicação que consome dados da API do StackOverflow (https://api.stackexchange.com/docs); 
* Imprimir esses dados na tela do navegador.

## Requisitos: ##

### Construir uma pagina HTML de acordo com a imagem ###

![modeloDeInterface.png](https://bitbucket.org/repo/Rbyaaj/images/3243587768-modeloDeInterface.png)

### Todos os campos são opcionais porém 'page' e 'rpp' devem sempre ser utilizados em conjunto ###

### Ao submeter o formulário, imprimir abaixo dele a lista de perguntas retornadas, sem recarregar a página. ###

### A lista deve conter: ###

1. O título da pergunta, com link para a mesma;

2. O nome do autor, com link para o perfil do mesmo;

3. A pontuação da pergunta.